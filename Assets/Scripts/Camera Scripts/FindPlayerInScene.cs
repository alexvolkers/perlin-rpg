using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class FindPlayerInScene : MonoBehaviour
{
    private SnappyPlayer player;
    private CinemachineVirtualCamera vcam;

    void Start()
    {
        player = FindObjectOfType<SnappyPlayer>();
        
        vcam = GetComponent<CinemachineVirtualCamera>();
        vcam.Follow = player.transform;
    }
}
