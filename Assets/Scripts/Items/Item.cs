using UnityEngine;


[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject
{
    [Header("Defaults")]
    [SerializeField] public new string name = "New Item";
    [SerializeField] public Sprite icon = null;
    [SerializeField] public bool isDefaultItem = false;
    [Header("Stacking items")]
    [SerializeField] public bool isStackable = false;
    [SerializeField] public int maxStackSize = 0;
    [Header("Items prefab for dropping / holding on cursor")]
    [SerializeField] public GameObject itemPrefab;

}
