using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    [SerializeField] public Item item;
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.collider.tag == "Player")
        {
            Pickup();    
        }
    }

    private void Pickup()
    {
        Debug.Log("Picked up " + item.name);
        bool wasPickedUp = Inventory.instance.AddItem(item);

        if (wasPickedUp)
        {
            Destroy(gameObject);
        }
        
    }
}
