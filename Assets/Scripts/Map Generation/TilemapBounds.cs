using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapBounds : MonoBehaviour
{
    private GridLayout gridLayout;
    private Tilemap tilemap;

    private void Awake()
    {
        gridLayout = transform.parent.GetComponentInParent<GridLayout>();
        tilemap = GetComponent<Tilemap>();
    }

    public bool isWalkable(Vector3 pos)
    {
        Vector3Int cellPos = gridLayout.WorldToCell(pos);
        //Debug.Log("name : " + tilemap.GetTile(cellPos).name + " & position : " + cellPos);
        var hasTile = tilemap.HasTile(cellPos);
        //Debug.Log(tilemap.HasTile(cellPos));
        //Debug.Log(tilemap.GetTile(cellPos).name);


        if (hasTile && tilemap.GetTile(cellPos).name != "RuleTileRock")
        {
            return true;
        }
        return false;
    }
    
}
