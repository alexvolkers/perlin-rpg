using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class TileMapPerlin : MonoBehaviour
{
    [Header("Things for creating sprite prefabs (Not used for tilemap)")]
    [SerializeField]private GameObject prefabWater;
    [SerializeField]private GameObject prefabGrass;
    [SerializeField]private GameObject prefabRocks;

    [Header("Things for the tilemap")]
    [SerializeField] private Tilemap _tilemap;
    [SerializeField] private RuleTile ruleTileWater;
    [SerializeField] private RuleTile ruleTileGrass;
    [SerializeField] private RuleTile ruleTileRock;

    [Header("Mapgeneration settings")]
    [SerializeField] private float zoomLevel = 7f;
    [SerializeField] private int xOffset = 0;
    [SerializeField] private int yOffset = 0;
    [SerializeField] private int mapWidth = 100;
    [SerializeField] private int mapHeight = 100;

    [Header("Enemy spawning thingies")]
    [SerializeField, Tooltip("1/this amount to spawn an enemy PER TILE")] private int chanceToSpawnOnTile = 20;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject enemyParent;

    [Header("Player prefab to spawn in the world")] 
    [SerializeField] private GameObject playerPrefab;

    //Used for OLD generation (not using tile rules)
    private Dictionary<int, GameObject> tileset;  //0 = water, 1 = grass, 2 = rocks
    private Dictionary<int, GameObject> tileGroups; // Used to generate children under the map generator to separate the different gamebjects  

    //new dictionary 
    private Dictionary<int, RuleTile> ruletiles;

    private List<List<int>> noiseGrid = new List<List<int>>(); //2d List to store the perlin noise
    private List<List<GameObject>> tileGrid = new List<List<GameObject>>(); // 2d List to store the gameobjects that we spawn

    private bool hasSpawnedPlayer = false;

    void Awake()
    {
        CreateTileset();
        CreateTileGroup();
        GenerateMap();
    }

    private void CreateTileset()
    {
        //For generating a grid without tileset and rules
        tileset = new Dictionary<int, GameObject>();
        
        tileset.Add(0, prefabWater);
        tileset.Add(1, prefabGrass);
        tileset.Add(2, prefabRocks);
        tileset.Add(3, prefabRocks);
        
        //For generation using tileset and rules
        ruletiles = new Dictionary<int, RuleTile>();
        ruletiles.Add(0, ruleTileWater);
        ruletiles.Add(1, ruleTileGrass);
        ruletiles.Add(2, ruleTileRock);
        ruletiles.Add(3, ruleTileRock);

    }

    private void CreateTileGroup()
    {
        tileGroups = new Dictionary<int, GameObject>();
        foreach (var prefabPair in tileset)
        {
            GameObject tileGroup = new GameObject(prefabPair.Value.name);
            tileGroup.transform.parent = gameObject.transform;
            tileGroup.transform.localPosition = new Vector3(0,0,0); //Reset position so we don't get any wonkyness.
            
            tileGroups.Add(prefabPair.Key, tileGroup);
        }
    }

    private void GenerateMap()
    {
        for (int x = 0; x < mapWidth; x++)
        {
            //Generate a new list for the INNER list in the 2D list
            noiseGrid.Add(new List<int>());
            tileGrid.Add(new List<GameObject>());
            
            for (int y = 0; y < mapHeight; y++)
            {
                int tileId = GetIdUsingPerlin(x, y);  // Returns a value between 0 and the amount of different tiles we have (2 in this case since we have 3 different prefabs)
                noiseGrid[x].Add(tileId);
                CreateTile(tileId, x, y);
            }
        }
    }

    //Generates the perlin noise at a specific x / y coordinate. 
    private int GetIdUsingPerlin(int x, int y)
    { 
       float rawPerlin = Mathf.PerlinNoise(((x - xOffset) / zoomLevel), ((y - yOffset) / zoomLevel));
       float clampedPerlin = Mathf.Clamp(rawPerlin, 0.0f, 1.0f);

       float scaledPerlin = clampedPerlin * tileset.Count; 
       
       // Scales the perlin to be between 0 and however many different tiles we have.
       if (scaledPerlin == tileset.Count)
       {
           scaledPerlin = tileset.Count - 1;
       }

       return Mathf.FloorToInt(scaledPerlin);
    }

    private void CreateTile(int tileId, int x, int y)
    {
        //Code that generates the grid WITHOUT tilemap 
        /*GameObject tilePrefab = tileset[tileId];
        GameObject tileGroup = tileGroups[tileId];

        GameObject tile = Instantiate(tilePrefab, tileGroup.transform);

        tile.name = $"tile_x{x}_y{y}";
        tile.transform.localPosition = new Vector3(x, y, 0);
        
        tileGrid[x].Add(tile);*/
        
        //Generate tiles WITH tileset + rules
        //_tilemap.SetTile(new Vector3Int(x,y,0), ruleTileRock);

        //randomly spawn enemies at the specificied tile
        SpawnEnemyOnTile(tileId, x, y);

        if (!hasSpawnedPlayer && x > (mapWidth / 3) && y > (mapHeight / 3) && ruletiles[tileId] == ruleTileGrass)
        {
            Instantiate(playerPrefab, new Vector3(x + 0.5f, y + 0.5f, 0), Quaternion.identity);
            hasSpawnedPlayer = true;
        }
        
        //Actual code for setting the tile
        _tilemap.SetTile(new Vector3Int(x,y,0), ruletiles[tileId]);
    }

    private void SpawnEnemyOnTile(int tileId, int x, int y)
    {
        int shouldWeSpawnAnEnemy = Random.Range(0, chanceToSpawnOnTile + 1);
        if (ruletiles[tileId] == ruleTileGrass)
        {
            if (shouldWeSpawnAnEnemy == chanceToSpawnOnTile)
            {
                GameObject enemyInstance =
                    Instantiate(enemyPrefab, new Vector3(x + 0.5f, y + 0.5f, 0), Quaternion.identity);
                enemyInstance.transform.parent = enemyParent.transform;
            }
        }
    }
}
