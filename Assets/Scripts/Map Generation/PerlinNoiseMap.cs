using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoiseMap : MonoBehaviour
{
    [SerializeField]private GameObject prefabWater;
    [SerializeField]private GameObject prefabGrass;
    [SerializeField]private GameObject prefabRocks;

    [SerializeField] private float zoomLevel = 7f;
    [SerializeField] private int xOffset = 0;
    [SerializeField] private int yOffset = 0;
    
    private Dictionary<int, GameObject> tileset;  //0 = water, 1 = grass, 2 = rocks
    private Dictionary<int, GameObject> tileGroups; // Used to generate children under the map generator to separate the different gamebjects  

    private int mapWidth = 50;
    private int mapHeight = 50;

    private List<List<int>> noiseGrid = new List<List<int>>(); //2d List to store the perlin noise
    private List<List<GameObject>> tileGrid = new List<List<GameObject>>(); // 2d List to store the gameobjects that we spawn

    void Start()
    {
        CreateTileset();
        CreateTileGroup();
        GenerateMap();
    }

    private void CreateTileset()
    {
        tileset = new Dictionary<int, GameObject>();
        
        tileset.Add(0, prefabWater);
        tileset.Add(1, prefabGrass);
        tileset.Add(2, prefabRocks);
        tileset.Add(3, prefabRocks);
    }

    private void CreateTileGroup()
    {
        tileGroups = new Dictionary<int, GameObject>();
        foreach (var prefabPair in tileset)
        {
            GameObject tileGroup = new GameObject(prefabPair.Value.name);
            tileGroup.transform.parent = gameObject.transform;
            tileGroup.transform.localPosition = new Vector3(0,0,0); //Reset position so we don't get any wonkyness.
            
            tileGroups.Add(prefabPair.Key, tileGroup);
        }
    }

    private void GenerateMap()
    {
        for (int x = 0; x < mapWidth; x++)
        {
            //Generate a new list for the INNER list in the 2D list
            noiseGrid.Add(new List<int>());
            tileGrid.Add(new List<GameObject>());
            
            for (int y = 0; y < mapHeight; y++)
            {
                int tileId = GetIdUsingPerlin(x, y);  // Returns a value between 0 and the amount of different tiles we have (2 in this case since we have 3 different prefabs)
                noiseGrid[x].Add(tileId);
                CreateTile(tileId, x, y);
            }
        }
    }

    //Generates the perlin noise at a specific x / y coordinate. 
    private int GetIdUsingPerlin(int x, int y)
    { 
       float rawPerlin = Mathf.PerlinNoise(((x - xOffset) / zoomLevel), ((y - yOffset) / zoomLevel));
       float clampedPerlin = Mathf.Clamp(rawPerlin, 0.0f, 1.0f);

       float scaledPerlin = clampedPerlin * tileset.Count; 
       // Scales the perlin to be between 0 and however many different tiles we have.
       


       if (scaledPerlin == tileset.Count)
       {
           scaledPerlin = tileset.Count - 1;
       }

       return Mathf.FloorToInt(scaledPerlin);
    }

    private void CreateTile(int tileId, int x, int y)
    {
        GameObject tilePrefab = tileset[tileId];
        GameObject tileGroup = tileGroups[tileId];

        GameObject tile = Instantiate(tilePrefab, tileGroup.transform);

        tile.name = $"tile_x{x}_y{y}";
        tile.transform.localPosition = new Vector3(x, y, 0);
        
        tileGrid[x].Add(tile);
    }
}
