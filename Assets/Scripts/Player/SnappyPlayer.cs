using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SnappyPlayer : MonoBehaviour
{
    [SerializeField] private float timeBetweenMoving = 0.2f;
    [SerializeField] private Sprite [] playerSprites;

    private SpriteRenderer spriterenderer;
    
    private TilemapBounds tilemap;

    private bool canMove = true;
    private Vector2 movement;
    private float timeOfLastMovement;


    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        MovePlayer();
    }

    private void Start()
    {
        tilemap = FindObjectOfType<TilemapBounds>();
        spriterenderer = GetComponent<SpriteRenderer>();
    }

    private void MovePlayer()
    {
        if (canMove)
        {
            //Horizontal movement
            //todo Refactor this to just one if statement for horizontal movement and one if statement for vert movement using movement.x inside the vector instead of a hardcoded 1 or -1
            if (movement.x > 0.5)
            {
                if (tilemap.isWalkable(transform.position + new Vector3(1,0,0)))
                {
                    canMove = false;
                    transform.Translate(new Vector2(1f, 0));
                    timeOfLastMovement = Time.time;
                }

            }
            else if (movement.x < -0.5)
            {
                if (tilemap.isWalkable(transform.position + new Vector3(-1f, 0, 0)))
                {
                    canMove = false;
                    transform.Translate(new Vector2(-1f, 0));
                    timeOfLastMovement = Time.time;
                }
            }  
            
            //Vert movement  (We can add a "canMove" check in this statement if we don't want player to be able to jump diagonally
            if (movement.y > 0.5)
            {
                spriterenderer.sprite = playerSprites[0];
                if (tilemap.isWalkable(transform.position + new Vector3(0, 1f, 0)))
                {
                    canMove = false;
                    transform.Translate(new Vector2(0, 1f));
                    timeOfLastMovement = Time.time;
                }

            }
            else if (movement.y < -0.5)
            {
                spriterenderer.sprite = playerSprites[1];
                if (tilemap.isWalkable(transform.position + new Vector3(0, -1f, 0)))
                {
                    canMove = false;
                    transform.Translate(new Vector2(0, -1f));
                    timeOfLastMovement = Time.time;
                }
            }
        }
        else
        {
            if (Time.time > timeOfLastMovement + timeBetweenMoving)
            {
                canMove = true;
            }
        }
        
        
    }
}
