using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private ParticleSystem attackParticle;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attack();
        }
    }

    private void Attack()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        
        var clickedX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        var clickedY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;
        
        //Aims our "weapon" towards where we clicked
        transform.right = new Vector3(clickedX, clickedY, 0) - transform.position;
        
        attackParticle.Emit(1);

    }
}
