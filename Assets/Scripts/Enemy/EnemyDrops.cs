using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDrops : MonoBehaviour
{
    [SerializeField] private List<GameObject> lootPool;

    public void DropItemOnDeath()
    {
        if (lootPool != null)
        {
            int itemIndex = Random.Range(0, lootPool.Count);
            GameObject itemToDrop = lootPool[itemIndex];
            GameObject item =  Instantiate(itemToDrop, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);

            item.name = itemToDrop.name;
        }
    } 

}
