using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{

    [SerializeField] private float maxHealth = 4f;
    [SerializeField] private Image healthBar;
    
    private float currentHealth;
    private EnemyDrops dropLoot;


    private void Start()
    {
        currentHealth = maxHealth;
        dropLoot = GetComponent<EnemyDrops>();
    }

    public void TakeDamage(float hitDamage)
    {
        currentHealth -= hitDamage;

        healthBar.fillAmount = currentHealth / maxHealth;
        
        if (currentHealth <= 0)
        {
            dropLoot.DropItemOnDeath();
            gameObject.SetActive(false);
        }
    }

}
