using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Security.Cryptography;
using UnityEngine;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;


[RequireComponent(typeof(EnemyHealth))]
public class SnappyEnemy : MonoBehaviour
{
    [SerializeField] private float secsBetweenMoves = 1f;

    private TilemapBounds tilemap;
    private Vector2 movement;
    private EnemyHealth myHealth;
    private EnemyDrops dropLoot;


    private void Start()
    {
        tilemap = FindObjectOfType<TilemapBounds>();
        StartCoroutine(MoveEnemyRandomly());
        myHealth = GetComponent<EnemyHealth>();
        dropLoot = GetComponent<EnemyDrops>();
    }

    //This should probably be handled on the player weapon so we can more easily change how much damage the player should do..
    //todo Make it so we can change the players damage
    private void OnParticleCollision(GameObject other)
    {
        if (other.name == "Player Attack")
        {
            myHealth.TakeDamage(1f);
        }
    }

    
    //Instantly kill the enemy is the player steps on it
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "Player")
        {   
            dropLoot.DropItemOnDeath();
            Destroy(this.gameObject);
        }
    }


    IEnumerator MoveEnemyRandomly()
    {
        int randDirectionInt;

        while (true)
        {   
        
            randDirectionInt = Random.Range(0, 4);

            switch (randDirectionInt)
            {
                case 0:
                    //Debug.Log("Random number: " + randDirectionInt);
                    MoveEnemy(new Vector3(0,1,0));
                    break;
                case 1:
                   //Debug.Log("Random number: " + randDirectionInt);
                    MoveEnemy(new Vector3(0,-1,0));
                    break;
                case 2:
                    //Debug.Log("Random number: " + randDirectionInt);
                    MoveEnemy(new Vector3(1,0,0));
                    break;
                case 3:
                    //Debug.Log("Random number: " + randDirectionInt);
                    MoveEnemy(new Vector3(-1,0,0));
                    break;
                default:
                    //Debug.Log("Random number: " + randDirectionInt);
                    MoveEnemy(new Vector3(1,0,0));
                    break;
            }

            secsBetweenMoves = Random.Range(0.1f, 1f);
            
            yield return new WaitForSeconds(secsBetweenMoves);
        }
    }

    private void MoveEnemy(Vector3 direction)
    {
        if (tilemap.isWalkable(transform.position + direction))
        {
            transform.Translate(direction);
        }
    }
}
