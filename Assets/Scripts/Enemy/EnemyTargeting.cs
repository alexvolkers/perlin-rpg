using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTargeting : MonoBehaviour
{

    //The projectile goes through the map becasue we set the Z-value of "Enemy targeter to a negative value".
    [SerializeField] private Transform target;
    [SerializeField] private float maxRange = 30f;
    [SerializeField] private ParticleSystem projectileParticle;

    void Start()
    {
        target = FindObjectOfType<SnappyPlayer>().transform;
    }

    void FixedUpdate()
    {
        AimAttack();
    }

    private void AimAttack()
    {
        float targetDistance = Vector3.Distance(transform.position, target.position);
        transform.right = target.position - transform.position;

        if (targetDistance < maxRange)
        {
            Attack(true);
        }
        else
        {
            Attack(false);
        }
    }

    private void Attack(bool enemyInRange)
    {
        if (!projectileParticle.isPlaying && enemyInRange)
        {
            projectileParticle.Play();
        }
        else if (projectileParticle.isPlaying && !enemyInRange)
        {
            projectileParticle.Stop();
        }
        
    }
    
}
