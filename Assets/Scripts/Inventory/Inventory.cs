using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{

    #region Singleton
    public static Inventory instance;
    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance of Inventory found!");
            return;
        }
        instance = this;
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    [SerializeField] private int inventorySpace = 20;
    [SerializeField] public List<Item> items = new List<Item>();

    public bool AddItem(Item item)
    {
        if (items.Count >= inventorySpace)
        {
            Debug.Log("Not enough space.");
            return false;
        }

        items.Add(item);
        InvokeOnItemChangedCallback();
        return true;
    }

    public void RemoveItem(Item item)
    {
        items.Remove(item);
        InvokeOnItemChangedCallback();
    }

    private void InvokeOnItemChangedCallback()
    {
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }
}
