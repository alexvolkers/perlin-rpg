using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;


public class InventorySlot : MonoBehaviour
{
   
   public Image icon;
   public Button removeButton;
   
   private Item item;
   private bool itemOnCursor = false;
   private GameObject itemToHold;
   private Vector2 mousePosition;
   private Vector2 itemPosition;

   //Spaghetti intensifies
   private void Update()
   {
      if (itemOnCursor)
      {
         mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
         itemPosition = mousePosition;
         itemToHold.transform.position = itemPosition;
         
         if (Input.GetKeyDown(KeyCode.Mouse0))
         {
            //Put back into inventory if we're holding mouse over inventory
            if (EventSystem.current.IsPointerOverGameObject())
            {
               //Code to put the item back, but deleting the item isn't working.
               //Inventory.instance.AddItem(itemToHold.GetComponent<ItemPickup>().item);
               return;
            }

            DropItemOnCursor();
         }
      }
   }

   public void AddItem(Item newItem)
   {
     
      item = newItem;

      icon.sprite = item.icon;
      icon.enabled = true;
      removeButton.interactable = true;
   }

   public void ClearSlot()
   {
      item = null;
      icon.sprite = null;
      icon.enabled = false;
      removeButton.interactable = false;
   }

   public void OnRemoveButtonClick()
   {
      Inventory.instance.RemoveItem(item);
   }

   public void PutItemOnCursor()
   {
      if (!itemOnCursor)
      {
         itemToHold = Instantiate(item.itemPrefab);
         itemToHold.GetComponent<CircleCollider2D>().enabled = false;
         itemToHold.layer = LayerMask.NameToLayer("UI");
         itemOnCursor = true;
      }
   }

   public void DropItemOnCursor()
   {
      Instantiate(item.itemPrefab, new Vector3(mousePosition.x, mousePosition.y, 0f), Quaternion.identity);
      Destroy(itemToHold);
      itemOnCursor = false;
            
      OnRemoveButtonClick();
   }
   

}
